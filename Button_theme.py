
BT1 = {"button_color": (0, 34, 51),
       "button_color_over": (0, 34, 51),
       "button_color_click": (0, 34, 51),
       "font_size": 22, "font_over_size": 30,
       "textcolor": (245, 229, 233), "textcolor_over": (245, 177, 17),
       "textcolor_click": (0, 0, 0)}

BT2 = {"button_color": (22, 102, 101),
       "button_color_over": (22, 102, 101),
       "button_color_click": (245, 177, 17), "font_size": 22,
       "font_over_size": 24, "textcolor": (245, 229, 233),
       "textcolor_over": (245, 177, 17),
       "textcolor_click": (0, 0, 0)}

BT_LIST = [BT1, BT2]

# to add a new theme: BT3 = { ... }
# replace  last line with BT_LIST = [BT1, BT2, BT3, ...]

