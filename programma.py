import pygame
import sys
from setting import *

import classi_priv


class Program():
    def __init__(self):
        pygame.init()
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((SCREEN_X, SCREEN_Y))
        pygame.display.set_caption(TITLE)

        self.sfondo = pygame.Surface((SCREEN_X, SCREEN_Y))
        self.sfondo.fill(pygame.Color(PALETTE[2]))


        self.running = True
        self.font = pygame.font.SysFont("comicsans",30)
        self.button_list = []
        self.button_out_list = []
        self.text_list = []


    def run(self):
        #INSERT BUTTON AND TEXT:
        # button: x position, y position, length, height, text, border param, button type (see button theme file)
        # text: text, font, color, x, y

        #some example:
        self.button_1a = classi_priv.bottone(50, 150, 100, 20, 'BUTTON TYPE 1a', 0, 0, 0, 0)
        self.button_list.append(self.button_1a)
        self.button_1b = classi_priv.bottone(50, 170, 100, 20, 'BUTTON TYPE 1b', 0, 0, 0, 0)
        self.button_list.append(self.button_1b)
        self.button_1c = classi_priv.bottone(50, 190, 100, 20, 'BUTTON TYPE 1c', 0, 0, 0, 0)
        self.button_list.append(self.button_1c)
        self.button_1d = classi_priv.bottone(50, 210, 100, 20, 'BUTTON TYPE 1d', 0, 0, 0, 0)
        self.button_list.append(self.button_1d)

        self.button_2a = classi_priv.bottone(250, 100, 200, 50, 'BUTTON TYPE 2a', 0, 0, 25, 25, 1)
        self.button_list.append(self.button_2a)
        self.button_2b = classi_priv.bottone(250, 200, 200, 50, 'BUTTON TYPE 2b', 25, 20, 20, 20, 1)
        self.button_list.append(self.button_2b)
        self.button_2c = classi_priv.bottone(250, 300, 200, 50, 'BUTTON TYPE 2c', 25, 20, 0, 0, 1)
        self.button_list.append(self.button_2c)
        self.button_2d = classi_priv.bottone(250, 400, 200, 50, 'BUTTON TYPE 2d', 0, 0, 0, 0, 1)
        self.button_list.append(self.button_2d)

        self.button_out = classi_priv.bottone(500, 300, 200, 50, 'BOTTONE OUT', 25, 20, 20, 20, 1)
        self.button_out_list.append(self.button_out)



        self.text_1 = classi_priv.testo('STOGRANDISSIMOCAZZO', self.font, PALETTE[6], 20, 20)
        self.text_list.append(self.text_1)




        while self.running:
            self.get_events()
            self.update()
            self.draw()
            self.clock.tick(FPS)
        pygame.quit()
        sys.exit



    def get_events(self):
        for event in pygame.event.get():
            pos = pygame.mouse.get_pos()
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                selfrunning = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                self.mousepressed = True
            else:
                self.mousepressed = False

            for b in self.button_list:
                b.controllo(pygame.mouse.get_pos())
            for b in self.button_out_list:
                b.controllo(pygame.mouse.get_pos())

            #enter the actions for each button
            if self.button_1a.mouse_over and self.mousepressed:
                print('button 1a pressed')
            if self.button_2a.mouse_over and self.mousepressed:
                print('button 2a pressed')



    def update(self):
        pass

    def draw(self):

        self.screen.blit(self.sfondo, (0, 0))

        for b in self.text_list:
            b.draw_text(self.screen)

        for b in self.button_list:
            b.draw(self.screen, self.mousepressed)

        for b in self.button_out_list:
            b.draw(self.screen, self.mousepressed, 1)

        pygame.display.update()
